(ns dev
  (:require [clojure.repl :refer :all]
            [hw.schema :as sch]
            [malli.generator :as mg]
            [clojure.string :as str]
            [hw.handlers :as h]
            [java-time :as time]
            [hw.main :as main]
            [juxt.clip.repl :refer [start stop reset set-init! system]]
            [hw.api :as api]))

(set-init! (constantly main/system-config))

(defn gen-inputs
  [separator]
  (->> (mg/sample sch/Record)
       (map #(str/join separator (vals %)))))

(defn gen-lines
  [separator]
  (->> (gen-inputs separator)
       (str/join "\n")))

(defn gen-sample-file
  "Generate a sample input file with name `filename`, with values delimited by `separator`
  and records delimited with newlines."
  [separator filename]
  (spit filename (str (gen-lines separator) "\n")))

(comment
  (->> (mg/sample sch/Record)
       (map #(str/join "|" (vals %))))


  (gen-sample-file "|" "pipes")
  (gen-sample-file "," "commas")
  (gen-sample-file " " "spaces")

  (def pipes (gen-inputs "|"))
  (def commas (gen-inputs ","))
  (def spaces (gen-inputs " "))
  (def all (concat pipes commas spaces))

  (api/app {:uri "/records"
            :request-method :post
            :body (first all)})



  (time/local-date "1939-05-07")
  (time/local-date? "1939-05-07")

  (time/format "M/d/YYYY" (time/local-date "1931-05-07"))
  "5/7/1931"


  )
