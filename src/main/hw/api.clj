(ns hw.api
  (:require [reitit.ring :as ring]
            [reitit.ring.middleware.muuntaja :as rrmm]
            [reitit.ring.middleware.parameters :as rrmp]
            [muuntaja.core :as m]
            [reitit.ring.coercion :as rrc]
            [reitit.coercion.malli :as rcm]
            [hw.handlers :as h]))

(defn log [req]
  {:status 200
   :body (pr-str (select-keys req [:params :body-params :form-params :query-params :path-params]))})

(defn my-params
  "For some reason rrmp/parameters-middleware isn't merging everything into :params, so
  I'm doing it myself."
  [handler]
  (fn [req]
    (let [{:keys [params body-params form-params query-params path-params]} req]
      (handler (assoc req :params (merge params
                                         query-params
                                         body-params
                                         form-params
                                         path-params))))))

(def router
  (ring/router
   [["/records"           {:post {:handler h/post-handler}}]
    ["/records/color"     {:get {:handler h/color-handler}}]
    ["/records/birthdate" {:get {:handler h/age-handler}}]
    ["/records/name"      {:get {:handler h/name-handler}}]
    ["/test"              {:get {:handler log}
                           :post {:handler log}}]]
   {:data {:muuntaja m/instance
           :coercion rcm/coercion
           :middleware [#_{:name :logtop
                         :wrap (fn [h] (fn [req] (println "top" (pr-str req)) (h req)))}
                        rrmp/parameters-middleware
                        rrmm/format-middleware
                        rrc/coerce-exceptions-middleware
                        rrc/coerce-request-middleware
                        rrc/coerce-response-middleware

                        {:name ::parameters
                         :wrap my-params}
                        #_{:name :logbottom
                         :wrap (fn [h] (fn [req] (println "bottom" (pr-str req)) (h req)))}]}}))

(def app
  (ring/ring-handler
   router
   (fn [_] {:status 404})))


(comment





  )
