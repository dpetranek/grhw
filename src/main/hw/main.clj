(ns hw.main
  (:require [hw.handlers :as h]
            [juxt.clip.core :as clip]
            [hw.server :as http])
  (:gen-class))

(def system-config
  {:components
   {:http {:start '(hw.server/start-server 3000)
           :stop '(hw.server/stop-server this)}}})

(defn -main
  "Accepts a relative or absolute path to a file, and a sort method."
  [& {:as args}]
  (println "starting..." (pr-str args))
  (if (get args "-server")
    (do (clip/start system-config)
        ;; block forever
        @(promise))
    (h/cli-handler args)))
