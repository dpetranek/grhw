(ns hw.handlers
  (:require [clojure.string :as str]
            [malli.core :as m]
            [malli.transform :as mt]
            [ring.util.response :as resp]
            [hw.schema :as sch]
            [hw.db :as db]
            [java-time :as time]))

(def columns
  [:last-name :first-name :email :favorite-color :date-of-birth])

(defn parse-line
  "Takes a line and converts into a Record. Returns `nil` if record is invalid."
  [line]
  (let [in (zipmap columns (str/split line #"\||,|\ "))
        candidate (m/decode sch/Record in mt/string-transformer)]
    (when (m/validate sch/Record candidate)
      candidate
      ;; (m/explain sch/Record candidate)
      )))

(comment
  (parse-line "C|Z|abc@|yello|1939-50-07")
  nil
  (parse-line "C|Z|abc@|yello|1939-05-07")
  {:last-name "C", :first-name "Z", :email "abc@", :favorite-color "yello", :date-of-birth "1939-05-07"}

  ["C" "Z" "abc@" "yello" "1939-05-07"]


  (m/decode sch/Record
            {:last-name "C" :first-name "Z" :email "abc@"
             :favorite-color "yello" :date-of-birth "1939-05-07"}
            mt/string-transformer)

  ;; invalid date? return nil (invalid)
  (m/decode sch/Record
            {:last-name "C" :first-name "Z" :email "abc@"
             :favorite-color "yello" :date-of-birth "1939-66-07"}
            mt/string-transformer)


  )

;; TODO: default lexicographic sort is a bit weird with casing

(defn sort-color
  [records]
  (->> records
       (sort-by :last-name)
       (sort-by :favorite-color)))

(defn sort-age
  [records]
  (sort-by :date-of-birth records))

(defn sort-name
  [records]
  (reverse (sort-by :last-name records)))

(defn print-nicely
  [records]
  (doseq [{:keys [last-name first-name email favorite-color date-of-birth]}
          records]
    ;; TODO: make output more useful once you find out what it's for
    (println last-name first-name email favorite-color
             (time/format "M/d/YYYY" (time/local-date date-of-birth)))))

(def sort-types #{"1" "2" "3"})

(defn validate-args
  "If :errors are not nil, there will be a valid :input and :sort-type in the map."
  [args]
  (if-let [{:keys [errors]} (-> {}
                                (cond-> (not (contains? args "-file")) (update :errors (fnil conj []) "Missing -file arg"))
                                (cond-> (not (contains? args "-sort")) (update :errors (fnil conj []) "Missing -sort arg"))
                                (not-empty))]
    {:errors {:args errors}}
    (let [file      (get args "-file")
          sort-type (get args "-sort")

          {file-error :error input :input}
          (try {:input (slurp file)}
               (catch Exception _ {:error (str "Invalid file path: " file)}))

          {sort-type-error :error}
          (if (sort-types sort-type)
            {:sort-type sort-type}
            {:error (str "Invalid sort type: " sort-type ", only 1, 2, or 3 works")})]
      {:errors    (-> {}
                      (cond-> file-error (assoc :file file-error))
                      (cond-> sort-type-error (assoc :sort sort-type-error))
                      (not-empty))
       :input     input
       :sort-type sort-type})))

(defn cli-handler
  [args]
  (let [{:keys [errors input sort-type]} (validate-args args)]
    (if errors
      (println errors)
      (let [records (->> (str/split-lines input)
                         (map parse-line)
                         (remove nil?))
            sorted (case sort-type
                     "1" (sort-color records)
                     "2" (sort-age records)
                     "3" (sort-name records))]
        (print-nicely sorted)
        sorted))))

(defn post-handler
  [{:keys [params] :as _req}]
  (if-let [candidate (:record params)]
    (if-let [record (parse-line candidate)]
      (do
        (swap! db/db conj record)
        {:status 200
         :body record})
      (resp/bad-request {:error (str "Incorrectly formatted record: " (pr-str (:record params)))}))
    (resp/bad-request {:error (str "Must submit record param in request body:" (pr-str params))})))

(defn color-handler
  [_]
  {:status 200
   :body (sort-color @db/db)})

(defn name-handler
  [_]
  {:status 200
   :body (sort-name @db/db)})

(defn age-handler
  [_]
  {:status 200
   :body (sort-age @db/db)})
