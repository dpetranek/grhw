(ns hw.server
  (:require [ring.adapter.jetty9 :as ring-http]
            [hw.api :as api]))

(defn start-server [port]
  (println "Starting Jetty server on port" port)
  (ring-http/run-jetty api/app {:port port :join? false}))

(defn stop-server [server]
  (println "Stopping Jetty server")
  (ring-http/stop-server server))


(comment
  (def server (start-server 3000))
  (stop-server server)

  )
