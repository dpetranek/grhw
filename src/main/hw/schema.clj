(ns hw.schema
  (:require [java-time :as time]
            [clojure.string :as str]
            [malli.generator :as mg]))

(defn local-date-decoder
  "Returns a valid YYYY-MM-DD date string or nil if we can't construct a LocalDate from
  the given string."
  [x]
  (try (str (time/local-date x))
       (catch Exception _)))

(defn local-date-string?
  [x]
  (and (string? x)
       (time/local-date? (try (time/local-date x)
                              (catch Exception _)))))

(defn gen-local-date-string
  [_]
  (str (time/minus (time/local-date)
                   (time/years (rand-int 100)))))

(defn valid-email?
  [x]
  (and (string? x)
       (str/includes? x "@")))

(def Name
  [:and string?
   [:not empty?]])

(defn gen-email
  [_]
  (str "abc@" (mg/generate Name)))

(def Email
  [:fn {:gen/fmap gen-email} valid-email?])

(def Dob
  [:fn {:gen/fmap gen-local-date-string}
   local-date-string?] )

(def Record
  "Internal representation of a record."
  [:map
   [:last-name Name]
   [:first-name Name]
   [:email Email]
   [:favorite-color {:gen/schema [:enum "red" "yellow" "blue"]} string?]
   [:date-of-birth {:decode/string local-date-decoder} Dob]])
