(ns hw.handlers-test
  (:require [hw.handlers :as h]
            [clojure.test :refer :all]
            [expectations.clojure.test :as e]))

(deftest validating-args
  (testing "receive error when missing args"
    (e/expect {:args ["Missing -file arg" "Missing -sort arg"]}
              (:errors (h/validate-args {}))))
  (testing "receive error when -file is spelled incorrectly"
    (e/expect {:args ["Missing -file arg"]}
              (:errors (h/validate-args  {"-filoo" "pipes" "-sort" "1"}))))
  (testing "receive error when file path is incorrect"
    (e/expect {:file "Invalid file path: nope"}
              (:errors (h/validate-args {"-file" "nope" "-sort" "1"}))))
  (testing "receive error when sort type is incorrect"
    (with-redefs [clojure.core/slurp (fn [& _] "abc")]
      (e/expect {:sort "Invalid sort type: nope, only 1, 2, or 3 works"}
                (:errors (h/validate-args {"-file" "okay" "-sort" "nope"})))))
  (testing "valid input is passed on with nil errors"
    (with-redefs [clojure.core/slurp (fn [& _] "abc")]
      (e/expect {:errors nil, :input "abc", :sort-type "1"}
                (h/validate-args {"-file" "okay" "-sort" "1"})))))

(deftest parsing
  (testing "pipe delimited lines can be parsed"
    (e/expect {:last-name "C" :first-name "Z" :email "abc@d.com" :favorite-color "yello" :date-of-birth "1939-05-07"}
              (h/parse-line "C|Z|abc@d.com|yello|1939-05-07")))

  (testing "comma delimited lines can be parsed"
    (e/expect {:last-name "C" :first-name "Z" :email "abc@d.com" :favorite-color "yello" :date-of-birth "1939-05-07"}
              (h/parse-line "C,Z,abc@d.com,yello,1939-05-07")))

  (testing "space delimited lines can be parsed"
    (e/expect {:last-name "C" :first-name "Z" :email "abc@d.com" :favorite-color "yello" :date-of-birth "1939-05-07"}
              (h/parse-line "C Z abc@d.com yello 1939-05-07")))

  (testing "invalid inputs return nil"
    ;; invalid date
    (e/expect nil (h/parse-line "C Z abc@d.com yello 1939-55-77"))
    ;; no "@" in email
    (e/expect nil (h/parse-line "C Z abcd.com yello 1939-05-07"))
    ;; blank first name
    (e/expect nil (h/parse-line "C  abcd.com yello 1939-05-07"))
    ;; blank last name
    (e/expect nil (h/parse-line " Z abcd.com yello 1939-05-07"))))

(deftest sorting
  (testing "sort1 sorts by favorite color, then last name ascending"
    (e/expect [{:favorite-color "blue" :last-name "Alice"}
               {:favorite-color "blue" :last-name "George"}
               {:favorite-color "blue" :last-name "Zorro"}
               {:favorite-color "green" :last-name "Alice"}
               {:favorite-color "green" :last-name "George"}
               {:favorite-color "red" :last-name "George"}
               {:favorite-color "red" :last-name "Zorro"}
               {:favorite-color "yellow" :last-name "George"}]
              (h/sort-color [{:favorite-color "red" :last-name "George"}
                             {:favorite-color "blue" :last-name "George"}
                             {:favorite-color "yellow" :last-name "George"}
                             {:favorite-color "green" :last-name "George"}
                             {:favorite-color "blue" :last-name "Alice"}
                             {:favorite-color "green" :last-name "Alice"}
                             {:favorite-color "red" :last-name "Zorro"}
                             {:favorite-color "blue" :last-name "Zorro"}])))

  (testing "sort2 sorts by birth date ascending"
    (e/expect [{:date-of-birth "1900-01-01"}
               {:date-of-birth "1950-01-01"}
               {:date-of-birth "2000-01-01"}]
              (h/sort-age [{:date-of-birth "2000-01-01"}
                           {:date-of-birth "1900-01-01"}
                           {:date-of-birth "1950-01-01"}])))

  (testing "sort3 sorts by last name descending"
    (e/expect [{:last-name "Zorro"}
               {:last-name "George"}
               {:last-name "Alice"}]
              (h/sort-name [{:last-name "George"}
                            {:last-name "Alice"}
                            {:last-name "Zorro"}]))))

(deftest cli
  (testing "properly discards invalid lines"
    (with-redefs [h/validate-args (fn [_] {:input (str "E,F,abc@xmP,blue,2003-05-10\n" ; valid
                                                       "l,f,email,green,2003-05-10\n" ; invalid
                                                       )
                                           :sort-type "3"})]
      (e/expect [{:last-name "E" :first-name "F" :email "abc@xmP" :favorite-color "blue" :date-of-birth "2003-05-10"}]
                (h/cli-handler {"-file" "foo" "-sort" "3"}))))

  (testing "properly formats dates as m/d/yyyy when printing"
    (let [out (atom nil)]
      (with-redefs [clojure.core/println (fn [& xs] (reset! out xs))
                    h/validate-args (fn [_] {:input "Last,First,email@a.x,blue,2003-05-10\n" :sort-type "2"})]
        (h/cli-handler {"-file" "foo" "-sort" "2"})
        (e/expect ["Last" "First" "email@a.x" "blue" "5/10/2003"]
                  @out)))))
